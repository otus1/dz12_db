﻿using Common;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Collections.Generic;

namespace EFConnect
{
    public class Repository : IRepository
    {
        private DataContext context;

        public Repository(string connectionString)
        {
            var optionBuilder = new DbContextOptionsBuilder<DataContext>();
            var option = optionBuilder.UseSqlite(connectionString).Options;
            context = new DataContext(option);
        }

        

        public void Create<T>(T entity) where T : Entity
        {
            var _context = context.Set<T>();
            _context.Add(entity);
            context.SaveChanges();
        }

        public void Delete<T>(T entity) where T : Entity
        {
            var _context = context.Set<T>();
            _context.Remove(entity);
            context.SaveChanges();
        }        

        public T Read<T>(long id) where T : Entity, new()
        {
            var _context = context.Set<T>();
            return _context.SingleOrDefault(x => x.Id == id);
        }

        public void Update<T>(T entity) where T : Entity
        {
            var _context = context.Set<T>();
            _context.Update(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }


        public User AddUser(string lastName, string firstName, DateTime birthDate)
        {
            User user = new User {LastName = lastName, FirstName = firstName, BirthDate = birthDate, Course=new Course() };
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }

        public Course AddСourse(string name, DateTime startDate, DateTime finishDate)
        {
            Course course = new Course { Name = name, StartDate = startDate, FinishDate = finishDate };
            context.Courses.Add(course);
            context.SaveChanges();
            return course;
        }

        public void JoinUserToCourse(User user, Course course)
        {
            user.Course = course;
            user.CourseId = course.Id;
            Update<User>(user);
        }

        public List<User> GetUserListInCourse(long id)
        {
            return Read<Course>(id).Users?.ToList(); 
        }

        public Lesson AddLesson(string name, DateTime evenDate, long courseId)
        {
            var lesson = new Lesson(name, evenDate, courseId);
            context.Lessons.Add(lesson);
            context.SaveChanges();
            return lesson;
        }

        public List<Lesson> GetLessonListInCourse(long id)
        {            
            return Read<Course>(id).Lessons?.ToList(); 
        }

        public Rating AddRating(long studentId, long lessonId, int ratingValue)
        {
            var item = new Rating(studentId, lessonId, ratingValue);
            context.Ratings.Add(item);
            context.SaveChanges();
            return item;
        }

        public List< Rating> GetRatingListInLesson(long id)
        {
            return Read<Lesson>(id).Rating?.ToList();
        }
    }
}
