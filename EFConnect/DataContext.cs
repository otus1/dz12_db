﻿using Common;
using Microsoft.EntityFrameworkCore;

namespace EFConnect
{
    public class DataContext :DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Rating> Ratings { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(x => x.Course);
            modelBuilder.Entity<Lesson>()
                .HasOne(x => x.Course);
            modelBuilder.Entity<Rating>()
                .HasOne(x => x.Lesson);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=OtusDB.db");
    }
}
