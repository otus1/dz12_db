﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common
{
    [Table("Lesson")]
    public class Lesson:Entity
    {
        [Column("Name")]
        public string Name { get; set; }

        [Column("EvenDate")]
        public DateTime EvenDate { get; set; }

        [Column("CourseId")]
        public long CourseId { get; set; }
        public Course Course { get; set; }

        public List<Rating> Rating { get; set; }


        public Lesson(string name, DateTime evenDate, long courseId)
        {
            Name = name;
            EvenDate = evenDate;
            CourseId = courseId;
        }
        public Lesson()
        {

        }

        public override string ToString()
        {
            return $"{EvenDate.ToString("dd.MM.yyyy")}: {Name}";
        }
    }
}


