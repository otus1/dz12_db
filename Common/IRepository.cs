﻿using System;
using System.Collections.Generic;

namespace Common
{
    public interface IRepository: IDisposable
    {
        void Create<T>(T entity) where T : Entity;
        T Read<T>(long id) where T : Entity, new();
        void Delete<T>(T entity) where T : Entity;
        void Update<T>(T entity) where T : Entity;

        User AddUser(string lastName, string firstName,  DateTime birthDate);
        Course AddСourse(string name, DateTime startDate, DateTime finishDate);
        void JoinUserToCourse(User user, Course course);
        List<User> GetUserListInCourse(long id);

        List<Lesson> GetLessonListInCourse(long id);    
        Lesson AddLesson(string name, DateTime evenDate, long courseId);

        Rating AddRating(long studentId, long lessonId, int ratingValue);
        List<Rating> GetRatingListInLesson(long id);
    }
}
