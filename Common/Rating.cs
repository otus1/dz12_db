﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common
{
    [Table("Rating")]
    public class Rating:Entity
    {
        [Column("StudentId")]
        public long StudentId { get; set; }
        public User User{ get; set; }

        [Column("LessonId")]
        public long LessonId { get; set; }
        public Lesson Lesson{ get; set; }

        [Column("RatingVAlue")]
        public int RatingValue { get; set; }

        public Rating(long studentId, long lessonId, int ratingValue)
        {
            StudentId = studentId;
            LessonId = lessonId;
            RatingValue = ratingValue;
        }
        public Rating()
        {

        }        
    }
}
