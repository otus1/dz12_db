﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common
{
    public abstract class Entity
    {
        [Column("Id")]
        public long Id { get; set; }
    }
}
