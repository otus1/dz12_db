﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common
{
    [Table("users")]
    public class User : Entity
    {   
        [Column("FirstName")]
        public string FirstName { get; set; }

        [Column("LastName")]
        public string LastName { get; set; }

        [Column("BirthDate")]
        public DateTime BirthDate { get; set; }

        [Column("CourseId")]
        public long CourseId { get; set; }
        public Course Course { get; set; }

        public override string ToString()
        {
            return $"{LastName} {FirstName} ({BirthDate.ToString("dd.MM.yyyy")})";
        }

    }
}
