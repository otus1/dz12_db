﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common
{
    public class Course: Entity
    {
        [Column("Name")]
        public string Name { get; set; }

        [Column("StartDate")]
        public DateTime StartDate { get; set; }

        [Column("FinishDate")]
        public DateTime FinishDate { get; set; }
        
        public List<User> Users { get; set; }
        public List<Lesson> Lessons { get; set; }

        public override string ToString()
        {
            return $"\"{Name}\" ({StartDate.ToString("dd.MM.yyyy")} - {FinishDate.ToString("dd.MM.yyyy")})";
        }
    }
}
