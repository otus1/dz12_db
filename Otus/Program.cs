﻿
using Common;

using EFConnect;
//using AdoConnect;

using System;

namespace Otus
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "Data Source=OtusDB.db";

            using IRepository repo = new Repository(connectionString);

            var user = repo.AddUser("Иванов", "Иван", DateTime.Parse("01.01.2010"));
            Console.WriteLine(user);

            var course = repo.AddСourse("C# для профи", DateTime.Parse("25.11.2019"),DateTime.Parse("25.05.2020"));
            Console.WriteLine(course);

            repo.JoinUserToCourse(user, course);

            Console.WriteLine("");
            Console.WriteLine($"Список студентов на курсе: {course.Name}");
            Console.WriteLine("====================");

            foreach (var el in repo.GetUserListInCourse(course.Id))
            {
                Console.WriteLine(el);
            }


            var lesson1 = repo.AddLesson("C# вводный курс",       DateTime.Parse("01.01.2020"), course.Id);
            var lesson2 = repo.AddLesson("C# Операторы и методы", DateTime.Parse("07.01.2020"), course.Id);


            repo.AddRating(user.Id, lesson1.Id, 5);
            repo.AddRating(user.Id, lesson2.Id, 4);

            Console.WriteLine("\n====================");
            foreach (var el in repo.GetLessonListInCourse(course.Id))
            {
                Console.WriteLine("\n"+el);
                foreach (var rating in repo.GetRatingListInLesson(el.Id))
                {
                    Console.WriteLine($"           > Rating = {rating.RatingValue} ({repo.Read<User>(rating.StudentId).FirstName})");                    
                }
            }
            Console.ReadKey();
        }
    }
}

