﻿using Common;
using System;
using System.Reflection;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Data.SQLite;
using System.Collections.Generic;

namespace AdoConnect
{
    public class Repository : IRepository
    {
        private SQLiteConnection _connection;

        public Repository(string connectionString)
        {
            _connection = new SQLiteConnection(connectionString);
            _connection.Open();
        }

        public void Create<T>(T entity) where T : Entity
        {
            var (f, v) = FieldsAndValues(typeof(T));
            var cmdString = $" INSERT INTO {GetTableName(typeof(T))} ({f}) VALUES ({v});"+
                            $" SELECT last_insert_rowid();";

            using var cmd = new SQLiteCommand(cmdString, _connection);
            foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                if (field != null && field != "Id")
                {
                    cmd.Parameters.AddWithValue(field, prop.GetValue(entity));
                }
            }
            entity.Id = (long)cmd.ExecuteScalar();
        }

        public void Delete<T>(T entity) where T : Entity
        {
            var cmdString = $"DELETE FROM {GetTableName(typeof(T))} WHERE Id = @id";
            using var cmd = new SQLiteCommand(cmdString, _connection);
            cmd.Parameters.AddWithValue("Id", entity.Id);
            cmd.ExecuteNonQuery();
        }        

        public T Read<T>(long id) where T : Entity, new()
        {
            T entity;
            var cmdString = $"SELECT * FROM {GetTableName(typeof(T))} WHERE Id = @Id";
            using (var cmd = new SQLiteCommand(cmdString, _connection))
            {
                cmd.Parameters.AddWithValue("id", id);
                using var reader = cmd.ExecuteReader();
                if (!reader.Read()) return null;

                entity = new T();
                foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                    if (field != null)
                        prop.SetValue(entity, reader.GetValue(reader.GetOrdinal(field)));
                }
            }
            return entity;
        }

        public void Update<T>(T entity) where T : Entity
        {
            var (f, v) = FieldsAndValues(typeof(T));
            var cmdString = $"UPDATE {GetTableName(typeof(T))} SET {f} WHERE Id= @id";
            using var cmd = new SQLiteCommand(cmdString, _connection);
            cmd.Parameters.AddWithValue("id", entity.Id);

            foreach (var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                if (field != null && field != "Id")
                    cmd.Parameters.AddWithValue(field, prop.GetValue(entity));
            }
            cmd.ExecuteNonQuery();        
        }
        public void Dispose()
        {
            _connection.Close();
        }

        private static string GetTableName(MemberInfo info)
        {
            var x = info.GetCustomAttribute<TableAttribute>();
            return x?.Name;
        }
        private (string, string) FieldsAndValues(IReflect t, bool withoutId = true, bool independent = true)
        {
            var props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var fields = new StringBuilder();
            var values = new StringBuilder();
            foreach (var prop in props)
            {
                var field = prop.GetCustomAttribute<ColumnAttribute>()?.Name;
                switch (field)
                {
                    case "Id" when withoutId:
                    case null:
                        continue;
                }

                if (fields.Length > 0)
                {
                    fields.Append(',');
                    if (independent)
                    {
                        values.Append(',');
                    }
                }
                fields.Append(field);
                if (independent)
                {
                    values.Append("@" + field);
                }
                else
                {
                    fields.Append(" = @" + field);
                }
            }
            return (fields.ToString(), values.ToString());
        }



        public User AddUser(string lastName, string firstName, DateTime birthDate)
        {
            var user = new User { LastName = lastName, FirstName = firstName, BirthDate = birthDate };
            Create<User>(user);
            return user;
        }

        public Course AddСourse(string name, DateTime startDate, DateTime finishDate)
        {
            var course = new Course { Name = name, StartDate = startDate, FinishDate = finishDate };
            Create<Course>(course);
            return course;
        }

        public void JoinUserToCourse(User user, Course course)
        {
            user.Course = course;
            Update<User>(user);            
        }

        public List<User> GetUserListInCourse(long id)
        {
            List<User> listUser = null;
            const string cmdString = "SELECT * FROM users WHERE id  = @id";
            using (var cmd = new SQLiteCommand(cmdString, _connection))
            {
                cmd.Parameters.AddWithValue("id", id);
                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var user = new User
                    {
                        Id = reader.GetFieldValue<long>(reader.GetOrdinal("Id")),
                        LastName= reader.GetFieldValue<string>(reader.GetOrdinal("LastName")),
                        FirstName = reader.GetFieldValue<string>(reader.GetOrdinal("FirstName")),
                        BirthDate = reader.GetFieldValue<DateTime>(reader.GetOrdinal("BirthDate"))
                    };
                    listUser.Add(user);
                }
            }
            return listUser;
        }

        public List<Lesson> GetLessonListInCourse(long id)
        {
            List<Lesson> list = null;
            const string cmdString = "SELECT * FROM lesson WHERE id  = @id";
            using (var cmd = new SQLiteCommand(cmdString, _connection))
            {
                cmd.Parameters.AddWithValue("id", id);
                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var item = new Lesson
                    {
                        Id = reader.GetFieldValue<long>(reader.GetOrdinal("Id")),
                        Name = reader.GetFieldValue<string>(reader.GetOrdinal("Name")),
                        EvenDate = reader.GetFieldValue<DateTime>(reader.GetOrdinal("EvenDate"))
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public Lesson AddLesson(string name, DateTime evenDate, long courseId)
        {
            var item = new Lesson (name,evenDate,courseId);
            Create<Lesson>(item);
            return item;
        }

        public Rating AddRating(long studentId, long lessonId, int ratingValue)
        {
            var item = new Rating(studentId,lessonId, ratingValue);
            Create<Rating>(item);
            return item;
        }

        public List<Rating> GetRatingListInLesson(long id)
        {

            List<Rating> list = null;
            const string cmdString = "SELECT * FROM rating WHERE id  = @id";
            using (var cmd = new SQLiteCommand(cmdString, _connection))
            {
                cmd.Parameters.AddWithValue("id", id);
                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var item = new Rating
                    {
                        Id = reader.GetFieldValue<long>(reader.GetOrdinal("Id")),
                        LessonId = reader.GetFieldValue<long>(reader.GetOrdinal("LessonId")),
                        StudentId = reader.GetFieldValue<long>(reader.GetOrdinal("StudentId")),
                        RatingValue= reader.GetFieldValue<int>(reader.GetOrdinal("RatingValue"))                        
                    };
                    list.Add(item);
                }
            }
            return list;
        }
    }
}

